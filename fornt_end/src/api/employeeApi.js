import axiosClient from "./axiosClient";

const employeeApi = {
    getAll: () => {
        const url = '/getDt';
        return axiosClient.get(url);
    },
    get: (id) => {
        const url = `/g/${id}`;
        return axiosClient.get(url);
    },
}

export default employeeApi