import React from "react";
import logo from "../images/logo.png"
import imgUser01 from "../images/users/img-01.jpg"
import img01 from "../images/img-01.png"
import imgProducts01 from "../images/products/img-01.jpg"
import imgProducts02 from "../images/products/img-02.jpg"
import imgProducts03 from "../images/products/img-03.jpg"
import { Link } from "react-router-dom";
// import vendor from "../js/vendor/jquery-library"




 function Header(){
    const[showCurrency,setShowCurrency]= React.useState('');
    const[showCart,setShowCart]= React.useState('');

    const handleShowCurrency = async () => {
        showCurrency!=''?setShowCurrency(''):setShowCurrency('open');
        console.log('Le THanh An');
     };

    const handleShowCart = async () => {
        showCart!=''?setShowCart(''):setShowCart('open');
        console.log('Le THanh An');
     };

    return(
        <>
        <header id="tg-header" className="tg-header tg-haslayout">
            <div className="tg-topbar">
                <div className="container">
                    <div className="row">
                        <div className="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                            <ul className="tg-addnav">
                                <li>
                                    <a href='#' onClick={e => e.preventDefault()}>
                                        <i className="icon-envelope"></i>
                                        <em>Contact</em>
                                    </a>
                                </li>
                                <li>
                                    <a href='#' onClick={e => e.preventDefault()}>
                                        <i className="icon-question-circle"></i>
                                        <em>Help</em>
                                    </a>
                                </li>
                            </ul>
                            <div className = {"dropdown tg-themedropdown tg-currencydropdown "+showCurrency}>
                                <a onClick={handleShowCurrency} href='#' id="tg-currenty" className="tg-btnthemedropdown"
                                   data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                    <i className="icon-earth"></i>
                                    <span>Currency</span>
                                </a>
                                <ul className="dropdown-menu tg-themedropdownmenu" aria-labelledby="tg-currenty">
                                    <li>
                                        <a href='#' onClick={e => e.preventDefault()}>
                                            <i>£</i>
                                            <span>British Pound</span>
                                        </a>
                                    </li>
                                    <li>
                                        <a href='#' onClick={e => e.preventDefault()}>
                                            <i>$</i>
                                            <span>Us Dollar</span>
                                        </a>
                                    </li>
                                    <li>
                                        <a href='#' onClick={e => e.preventDefault()}>
                                            <i>€</i>
                                            <span>Euro</span>
                                        </a>
                                    </li>
                                </ul>
                            </div>
                            <div className="tg-userlogin">
                                <figure><a href='#' onClick={e => e.preventDefault()}><img src={imgUser01}
                                                                           alt="image description"/></a></figure>
                                <span>Hi, John</span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div className="tg-middlecontainer">
                <div className="container">
                    <div className="row">
                        <div className="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                            <strong className="tg-logo"><a href="i#"><img src = {logo}
                                                                                    alt="company name here"/></a></strong>
                            <div className="tg-wishlistandcart">
                                <div className="dropdown tg-themedropdown tg-wishlistdropdown">
                                    <a href='#' onClick={e => e.preventDefault()} id="tg-wishlisst" className="tg-btnthemedropdown"
                                       data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                        <span className="tg-themebadge">3</span>
                                        <i className="icon-heart"></i>
                                        <span>Wishlist</span>
                                    </a>
                                    <div className="dropdown-menu tg-themedropdownmenu" aria-labelledby="tg-wishlisst">
                                        <div className="tg-description"><p>No products were added to the wishlist!</p>
                                        </div>
                                    </div>
                                </div>
                                <div className={"dropdown tg-themedropdown tg-minicartdropdown " + showCart}>

                                    <a  href='#' onClick={handleShowCart} id="tg-minicart" className="tg-btnthemedropdown"
                                       data-toggle="dropdown" aria-haspopup ="true" aria-expanded ="false">
                                        <span className="tg-themebadge">3</span>
                                        <i className="icon-cart"></i>
                                        <span>$123.00</span>
                                    </a>
                                    <div className={"dropdown-menu tg-themedropdownmenu"} aria-labelledby="tg-minicart"
                                         style={{
                                        // top: '100px',
                                        right: '0',
                                        left: 'auto',
                                        border: '0',
                                        'border-top': '5px solid #77b748',
                                        'z-index': '10',
                                        width: '330px',
                                        padding: '15px',
                                        display: 'block',
                                        margin: '25px 0 0'
                                    }}>
                                        <div className="tg-minicartbody">
                                            <div className="tg-minicarproduct">
                                                <figure>
                                                    <img src={imgProducts01} alt="image description"/>
                                                </figure>
                                                <div className="tg-minicarproductdata">
                                                    <h5><a href='#' onClick={e => e.preventDefault()}>Our State Fair Is A Great
                                                        Function</a></h5>
                                                    <h6><a href='#' onClick={e => e.preventDefault()}>$ 12.15</a></h6>
                                                </div>
                                            </div>
                                            <div className="tg-minicarproduct">
                                                <figure>
                                                    <img src={imgProducts02} alt="image description"/>

                                                </figure>
                                                <div className="tg-minicarproductdata">
                                                    <h5><a href='#' onClick={e => e.preventDefault()}>Bring Me To Light</a></h5>
                                                    <h6><a href='#' onClick={e => e.preventDefault()}>$ 12.15</a></h6>
                                                </div>
                                            </div>
                                            <div className="tg-minicarproduct">
                                                <figure>
                                                    <img src={imgProducts03} alt="image description"/>

                                                </figure>
                                                <div className="tg-minicarproductdata">
                                                    <h5><a href='#' onClick={e => e.preventDefault()}>Have Faith In Your Soul</a></h5>
                                                    <h6><a href='#' onClick={e => e.preventDefault()}>$ 12.15</a></h6>
                                                </div>
                                            </div>
                                        </div>
                                        <div className="tg-minicartfoot">
                                            <a className="tg-btnemptycart" href='#' onClick={e => e.preventDefault()}>
                                                <i className="fa fa-trash-o"></i>
                                                <span>Clear Your Cart</span>
                                            </a>
                                            <span className="tg-subtotal">Subtotal: <strong>35.78</strong></span>
                                            <div className="tg-btns">
                                                <a className="tg-btn tg-active" href='#' onClick={e => e.preventDefault()}>View Cart</a>
                                                <a className="tg-btn" href='#' onClick={e => e.preventDefault()}>Checkout</a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div className="tg-searchbox">
                                <form className="tg-formtheme tg-formsearch">
                                    <fieldset>
                                        <input type="text" name="search" className="typeahead form-control"
                                               placeholder="Search by title, author, keyword, ISBN..."/>
                                            <button type="submit"><i className="icon-magnifier"></i></button>
                                    </fieldset>
                                    <a href='#' onClick={e => e.preventDefault()}>+ Advanced Search</a>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div className="tg-navigationarea">
                <div className="container">
                    <div className="row">
                        <div className="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                            <nav id="tg-nav" className="tg-nav">
                                <div className="navbar-header">
                                    <button type="button" className="navbar-toggle collapsed" data-toggle="collapse"
                                            data-target="#tg-navigation" aria-expanded="false">
                                        <span className="sr-only">Toggle navigation</span>
                                        <span className="icon-bar"></span>
                                        <span className="icon-bar"></span>
                                        <span className="icon-bar"></span>
                                    </button>
                                </div>
                                <div id="tg-navigation" className="collapse navbar-collapse tg-navigation">
                                    <ul>
                                        <li className="menu-item-has-children menu-item-has-mega-menu">
                                            <a href='#' onClick={e => e.preventDefault()}>All Categories</a>
                                            <div className="mega-menu">
                                                <ul className="tg-themetabnav" role="tablist">
                                                    <li role="presentation" className="active">
                                                        <a href="#artandphotography" aria-controls="artandphotography"
                                                           role="tab" data-toggle="tab">Art &amp; Photography</a>
                                                    </li>
                                                    <li role="presentation">
                                                        <a href="#biography" aria-controls="biography" role="tab"
                                                           data-toggle="tab">Biography</a>
                                                    </li>
                                                    <li role="presentation">
                                                        <a href="#childrensbook" aria-controls="childrensbook"
                                                           role="tab" data-toggle="tab">Children’s Book</a>
                                                    </li>
                                                    <li role="presentation">
                                                        <a href="#craftandhobbies" aria-controls="craftandhobbies"
                                                           role="tab" data-toggle="tab">Craft &amp; Hobbies</a>
                                                    </li>
                                                    <li role="presentation">
                                                        <a href="#crimethriller" aria-controls="crimethriller"
                                                           role="tab" data-toggle="tab">Crime &amp; Thriller</a>
                                                    </li>
                                                    <li role="presentation">
                                                        <a href="#fantasyhorror" aria-controls="fantasyhorror"
                                                           role="tab" data-toggle="tab">Fantasy &amp; Horror</a>
                                                    </li>
                                                    <li role="presentation">
                                                        <a href="#fiction" aria-controls="fiction" role="tab"
                                                           data-toggle="tab">Fiction</a>
                                                    </li>
                                                    <li role="presentation">
                                                        <a href="#fooddrink" aria-controls="fooddrink" role="tab"
                                                           data-toggle="tab">Food &amp; Drink</a>
                                                    </li>
                                                    <li role="presentation">
                                                        <a href="#graphicanimemanga" aria-controls="graphicanimemanga"
                                                           role="tab" data-toggle="tab">Graphic, Anime &amp; Manga</a>
                                                    </li>
                                                    <li role="presentation">
                                                        <a href="#sciencefiction" aria-controls="sciencefiction"
                                                           role="tab" data-toggle="tab">Science Fiction</a>
                                                    </li>
                                                </ul>
                                                <div className="tab-content tg-themetabcontent">
                                                    <div role="tabpanel" className="tab-pane active"
                                                         id="artandphotography">
                                                        <ul>
                                                            <li>
                                                                <div className="tg-linkstitle">
                                                                    <h2>Architecture</h2>
                                                                </div>
                                                                <ul>
                                                                    <li><a href="#">Tough As Nails</a></li>
                                                                    <li><a href="#">Pro Grease Monkey</a>
                                                                    </li>
                                                                    <li><a href="#">Building Memories</a>
                                                                    </li>
                                                                    <li><a href="#">Bulldozer Boyz</a></li>
                                                                    <li><a href="#">Build Or Leave On Us</a>
                                                                    </li>
                                                                </ul>
                                                                <a className="tg-btnviewall" href="#">View
                                                                    All</a>
                                                            </li>
                                                            <li>
                                                                <div className="tg-linkstitle">
                                                                    <h2>Art Forms</h2>
                                                                </div>
                                                                <ul>
                                                                    <li><a href="#">Consectetur
                                                                        adipisicing</a></li>
                                                                    <li><a href="#">Aelit sed do eiusmod</a>
                                                                    </li>
                                                                    <li><a href="#">Tempor incididunt
                                                                        labore</a></li>
                                                                    <li><a href="#">Dolore magna aliqua</a>
                                                                    </li>
                                                                    <li><a href="#">Ut enim ad minim</a>
                                                                    </li>
                                                                </ul>
                                                                <a className="tg-btnviewall" href="#">View
                                                                    All</a>
                                                            </li>
                                                            <li>
                                                                <div className="tg-linkstitle">
                                                                    <h2>History</h2>
                                                                </div>
                                                                <ul>
                                                                    <li><a href="#">Veniam quis nostrud</a>
                                                                    </li>
                                                                    <li><a href="#">Exercitation</a></li>
                                                                    <li><a href="#">Laboris nisi ut
                                                                        aliuip</a></li>
                                                                    <li><a href="#">Commodo conseat</a></li>
                                                                    <li><a href="#">Duis aute irure</a></li>
                                                                </ul>
                                                                <a className="tg-btnviewall" href="#">View
                                                                    All</a>
                                                            </li>
                                                        </ul>
                                                        <ul>
                                                            <li>
                                                                <figure><img src={img01}
                                                                             alt="image description"/></figure>
                                                                <div className="tg-textbox">
                                                                    <h3>More Than<span>12,0657,53</span>Books Collection
                                                                    </h3>
                                                                    <div className="tg-description">
                                                                        <p>Consectetur adipisicing elit sed doe eiusmod
                                                                            tempor incididunt laebore toloregna aliqua
                                                                            enim.</p>
                                                                    </div>
                                                                    <a className="tg-btn" href="#">view
                                                                        all</a>
                                                                </div>
                                                            </li>
                                                        </ul>
                                                    </div>
                                                    <div role="tabpanel" className="tab-pane" id="biography">
                                                        <ul>
                                                            <li>
                                                                <div className="tg-linkstitle">
                                                                    <h2>History</h2>
                                                                </div>
                                                                <ul>
                                                                    <li><a href="#">Veniam quis nostrud</a>
                                                                    </li>
                                                                    <li><a href="#">Exercitation</a></li>
                                                                    <li><a href="#">Laboris nisi ut
                                                                        aliuip</a></li>
                                                                    <li><a href="#">Commodo conseat</a></li>
                                                                    <li><a href="#">Duis aute irure</a></li>
                                                                </ul>
                                                                <a className="tg-btnviewall" href="#">View
                                                                    All</a>
                                                            </li>
                                                            <li>
                                                                <div className="tg-linkstitle">
                                                                    <h2>Architecture</h2>
                                                                </div>
                                                                <ul>
                                                                    <li><a href="#">Tough As Nails</a></li>
                                                                    <li><a href="#">Pro Grease Monkey</a>
                                                                    </li>
                                                                    <li><a href="#">Building Memories</a>
                                                                    </li>
                                                                    <li><a href="#">Bulldozer Boyz</a></li>
                                                                    <li><a href="#">Build Or Leave On Us</a>
                                                                    </li>
                                                                </ul>
                                                                <a className="tg-btnviewall" href="#">View
                                                                    All</a>
                                                            </li>
                                                            <li>
                                                                <div className="tg-linkstitle">
                                                                    <h2>Art Forms</h2>
                                                                </div>
                                                                <ul>
                                                                    <li><a href="#">Consectetur
                                                                        adipisicing</a></li>
                                                                    <li><a href="#">Aelit sed do eiusmod</a>
                                                                    </li>
                                                                    <li><a href="#">Tempor incididunt
                                                                        labore</a></li>
                                                                    <li><a href="#">Dolore magna aliqua</a>
                                                                    </li>
                                                                    <li><a href="#">Ut enim ad minim</a>
                                                                    </li>
                                                                </ul>
                                                                <a className="tg-btnviewall" href="#">View
                                                                    All</a>
                                                            </li>
                                                        </ul>
                                                        <ul>
                                                            <li>
                                                                <figure><img src={img01}
                                                                             alt="image description"/></figure>
                                                                <div className="tg-textbox">
                                                                    <h3>More Than<span>12,0657,53</span>Books Collection
                                                                    </h3>
                                                                    <div className="tg-description">
                                                                        <p>Consectetur adipisicing elit sed doe eiusmod
                                                                            tempor incididunt laebore toloregna aliqua
                                                                            enim.</p>
                                                                    </div>
                                                                    <a className="tg-btn" href="#">view
                                                                        all</a>
                                                                </div>
                                                            </li>
                                                        </ul>
                                                    </div>
                                                    <div role="tabpanel" className="tab-pane" id="childrensbook">
                                                        <ul>
                                                            <li>
                                                                <div className="tg-linkstitle">
                                                                    <h2>Architecture</h2>
                                                                </div>
                                                                <ul>
                                                                    <li><a href="#">Tough As Nails</a></li>
                                                                    <li><a href="#">Pro Grease Monkey</a>
                                                                    </li>
                                                                    <li><a href="#">Building Memories</a>
                                                                    </li>
                                                                    <li><a href="#">Bulldozer Boyz</a></li>
                                                                    <li><a href="#">Build Or Leave On Us</a>
                                                                    </li>
                                                                </ul>
                                                                <a className="tg-btnviewall" href="#">View
                                                                    All</a>
                                                            </li>
                                                            <li>
                                                                <div className="tg-linkstitle">
                                                                    <h2>Art Forms</h2>
                                                                </div>
                                                                <ul>
                                                                    <li><a href="#">Consectetur
                                                                        adipisicing</a></li>
                                                                    <li><a href="#">Aelit sed do eiusmod</a>
                                                                    </li>
                                                                    <li><a href="#">Tempor incididunt
                                                                        labore</a></li>
                                                                    <li><a href="#">Dolore magna aliqua</a>
                                                                    </li>
                                                                    <li><a href="#">Ut enim ad minim</a>
                                                                    </li>
                                                                </ul>
                                                                <a className="tg-btnviewall" href="#">View
                                                                    All</a>
                                                            </li>
                                                            <li>
                                                                <div className="tg-linkstitle">
                                                                    <h2>History</h2>
                                                                </div>
                                                                <ul>
                                                                    <li><a href="#">Veniam quis nostrud</a>
                                                                    </li>
                                                                    <li><a href="#">Exercitation</a></li>
                                                                    <li><a href="#">Laboris nisi ut
                                                                        aliuip</a></li>
                                                                    <li><a href="#">Commodo conseat</a></li>
                                                                    <li><a href="#">Duis aute irure</a></li>
                                                                </ul>
                                                                <a className="tg-btnviewall" href="#">View
                                                                    All</a>
                                                            </li>
                                                        </ul>
                                                        <ul>
                                                            <li>
                                                                <figure><img src={img01}
                                                                             alt="image description"/></figure>
                                                                <div className="tg-textbox">
                                                                    <h3>More Than<span>12,0657,53</span>Books Collection
                                                                    </h3>
                                                                    <div className="tg-description">
                                                                        <p>Consectetur adipisicing elit sed doe eiusmod
                                                                            tempor incididunt laebore toloregna aliqua
                                                                            enim.</p>
                                                                    </div>
                                                                    <a className="tg-btn" href="#">view
                                                                        all</a>
                                                                </div>
                                                            </li>
                                                        </ul>
                                                    </div>
                                                    <div role="tabpanel" className="tab-pane" id="craftandhobbies">
                                                        <ul>
                                                            <li>
                                                                <div className="tg-linkstitle">
                                                                    <h2>History</h2>
                                                                </div>
                                                                <ul>
                                                                    <li><a href="#">Veniam quis nostrud</a>
                                                                    </li>
                                                                    <li><a href="#">Exercitation</a></li>
                                                                    <li><a href="#">Laboris nisi ut
                                                                        aliuip</a></li>
                                                                    <li><a href="#">Commodo conseat</a></li>
                                                                    <li><a href="#">Duis aute irure</a></li>
                                                                </ul>
                                                                <a className="tg-btnviewall" href="#">View
                                                                    All</a>
                                                            </li>
                                                            <li>
                                                                <div className="tg-linkstitle">
                                                                    <h2>Architecture</h2>
                                                                </div>
                                                                <ul>
                                                                    <li><a href="#">Tough As Nails</a></li>
                                                                    <li><a href="#">Pro Grease Monkey</a>
                                                                    </li>
                                                                    <li><a href="#">Building Memories</a>
                                                                    </li>
                                                                    <li><a href="#">Bulldozer Boyz</a></li>
                                                                    <li><a href="#">Build Or Leave On Us</a>
                                                                    </li>
                                                                </ul>
                                                                <a className="tg-btnviewall" href="#">View
                                                                    All</a>
                                                            </li>
                                                            <li>
                                                                <div className="tg-linkstitle">
                                                                    <h2>Art Forms</h2>
                                                                </div>
                                                                <ul>
                                                                    <li><a href="#">Consectetur
                                                                        adipisicing</a></li>
                                                                    <li><a href="#">Aelit sed do eiusmod</a>
                                                                    </li>
                                                                    <li><a href="#">Tempor incididunt
                                                                        labore</a></li>
                                                                    <li><a href="#">Dolore magna aliqua</a>
                                                                    </li>
                                                                    <li><a href="#">Ut enim ad minim</a>
                                                                    </li>
                                                                </ul>
                                                                <a className="tg-btnviewall" href="#">View
                                                                    All</a>
                                                            </li>
                                                        </ul>
                                                        <ul>
                                                            <li>
                                                                <figure><img src={img01}
                                                                             alt="image description"/></figure>
                                                                <div className="tg-textbox">
                                                                    <h3>More Than<span>12,0657,53</span>Books Collection
                                                                    </h3>
                                                                    <div className="tg-description">
                                                                        <p>Consectetur adipisicing elit sed doe eiusmod
                                                                            tempor incididunt laebore toloregna aliqua
                                                                            enim.</p>
                                                                    </div>
                                                                    <a className="tg-btn" href="#">view
                                                                        all</a>
                                                                </div>
                                                            </li>
                                                        </ul>
                                                    </div>
                                                    <div role="tabpanel" className="tab-pane" id="crimethriller">
                                                        <ul>
                                                            <li>
                                                                <div className="tg-linkstitle">
                                                                    <h2>Architecture</h2>
                                                                </div>
                                                                <ul>
                                                                    <li><a href="#">Tough As Nails</a></li>
                                                                    <li><a href="#">Pro Grease Monkey</a>
                                                                    </li>
                                                                    <li><a href="#">Building Memories</a>
                                                                    </li>
                                                                    <li><a href="#">Bulldozer Boyz</a></li>
                                                                    <li><a href="#">Build Or Leave On Us</a>
                                                                    </li>
                                                                </ul>
                                                                <a className="tg-btnviewall" href="#">View
                                                                    All</a>
                                                            </li>
                                                            <li>
                                                                <div className="tg-linkstitle">
                                                                    <h2>Art Forms</h2>
                                                                </div>
                                                                <ul>
                                                                    <li><a href="#">Consectetur
                                                                        adipisicing</a></li>
                                                                    <li><a href="#">Aelit sed do eiusmod</a>
                                                                    </li>
                                                                    <li><a href="#">Tempor incididunt
                                                                        labore</a></li>
                                                                    <li><a href="#">Dolore magna aliqua</a>
                                                                    </li>
                                                                    <li><a href="#">Ut enim ad minim</a>
                                                                    </li>
                                                                </ul>
                                                                <a className="tg-btnviewall" href="#">View
                                                                    All</a>
                                                            </li>
                                                            <li>
                                                                <div className="tg-linkstitle">
                                                                    <h2>History</h2>
                                                                </div>
                                                                <ul>
                                                                    <li><a href="#">Veniam quis nostrud</a>
                                                                    </li>
                                                                    <li><a href="#">Exercitation</a></li>
                                                                    <li><a href="#">Laboris nisi ut
                                                                        aliuip</a></li>
                                                                    <li><a href="#">Commodo conseat</a></li>
                                                                    <li><a href="#">Duis aute irure</a></li>
                                                                </ul>
                                                                <a className="tg-btnviewall" href="#">View
                                                                    All</a>
                                                            </li>
                                                        </ul>
                                                        <ul>
                                                            <li>
                                                                <figure><img src={img01}
                                                                             alt="image description"/></figure>
                                                                <div className="tg-textbox">
                                                                    <h3>More Than<span>12,0657,53</span>Books Collection
                                                                    </h3>
                                                                    <div className="tg-description">
                                                                        <p>Consectetur adipisicing elit sed doe eiusmod
                                                                            tempor incididunt laebore toloregna aliqua
                                                                            enim.</p>
                                                                    </div>
                                                                    <a className="tg-btn" href="#">view
                                                                        all</a>
                                                                </div>
                                                            </li>
                                                        </ul>
                                                    </div>
                                                    <div role="tabpanel" className="tab-pane" id="fantasyhorror">
                                                        <ul>
                                                            <li>
                                                                <div className="tg-linkstitle">
                                                                    <h2>History</h2>
                                                                </div>
                                                                <ul>
                                                                    <li><a href="#">Veniam quis nostrud</a>
                                                                    </li>
                                                                    <li><a href="#">Exercitation</a></li>
                                                                    <li><a href="#">Laboris nisi ut
                                                                        aliuip</a></li>
                                                                    <li><a href="#">Commodo conseat</a></li>
                                                                    <li><a href="#">Duis aute irure</a></li>
                                                                </ul>
                                                                <a className="tg-btnviewall" href="#">View
                                                                    All</a>
                                                            </li>
                                                            <li>
                                                                <div className="tg-linkstitle">
                                                                    <h2>Architecture</h2>
                                                                </div>
                                                                <ul>
                                                                    <li><a href="#">Tough As Nails</a></li>
                                                                    <li><a href="#">Pro Grease Monkey</a>
                                                                    </li>
                                                                    <li><a href="#">Building Memories</a>
                                                                    </li>
                                                                    <li><a href="#">Bulldozer Boyz</a></li>
                                                                    <li><a href="#">Build Or Leave On Us</a>
                                                                    </li>
                                                                </ul>
                                                                <a className="tg-btnviewall" href="#">View
                                                                    All</a>
                                                            </li>
                                                            <li>
                                                                <div className="tg-linkstitle">
                                                                    <h2>Art Forms</h2>
                                                                </div>
                                                                <ul>
                                                                    <li><a href="#">Consectetur
                                                                        adipisicing</a></li>
                                                                    <li><a href="#">Aelit sed do eiusmod</a>
                                                                    </li>
                                                                    <li><a href="#">Tempor incididunt
                                                                        labore</a></li>
                                                                    <li><a href="#">Dolore magna aliqua</a>
                                                                    </li>
                                                                    <li><a href="#">Ut enim ad minim</a>
                                                                    </li>
                                                                </ul>
                                                                <a className="tg-btnviewall" href="#">View
                                                                    All</a>
                                                            </li>
                                                        </ul>
                                                        <ul>
                                                            <li>
                                                                <figure><img src={img01}
                                                                             alt="image description"/></figure>
                                                                <div className="tg-textbox">
                                                                    <h3>More Than<span>12,0657,53</span>Books Collection
                                                                    </h3>
                                                                    <div className="tg-description">
                                                                        <p>Consectetur adipisicing elit sed doe eiusmod
                                                                            tempor incididunt laebore toloregna aliqua
                                                                            enim.</p>
                                                                    </div>
                                                                    <a className="tg-btn" href="#">view
                                                                        all</a>
                                                                </div>
                                                            </li>
                                                        </ul>
                                                    </div>
                                                    <div role="tabpanel" className="tab-pane" id="fiction">
                                                        <ul>
                                                            <li>
                                                                <div className="tg-linkstitle">
                                                                    <h2>Architecture</h2>
                                                                </div>
                                                                <ul>
                                                                    <li><a href="#">Tough As Nails</a></li>
                                                                    <li><a href="#">Pro Grease Monkey</a>
                                                                    </li>
                                                                    <li><a href="#">Building Memories</a>
                                                                    </li>
                                                                    <li><a href="#">Bulldozer Boyz</a></li>
                                                                    <li><a href="#">Build Or Leave On Us</a>
                                                                    </li>
                                                                </ul>
                                                                <a className="tg-btnviewall" href="#">View
                                                                    All</a>
                                                            </li>
                                                            <li>
                                                                <div className="tg-linkstitle">
                                                                    <h2>Art Forms</h2>
                                                                </div>
                                                                <ul>
                                                                    <li><a href="#">Consectetur
                                                                        adipisicing</a></li>
                                                                    <li><a href="#">Aelit sed do eiusmod</a>
                                                                    </li>
                                                                    <li><a href="#">Tempor incididunt
                                                                        labore</a></li>
                                                                    <li><a href="#">Dolore magna aliqua</a>
                                                                    </li>
                                                                    <li><a href="#">Ut enim ad minim</a>
                                                                    </li>
                                                                </ul>
                                                                <a className="tg-btnviewall" href="#">View
                                                                    All</a>
                                                            </li>
                                                            <li>
                                                                <div className="tg-linkstitle">
                                                                    <h2>History</h2>
                                                                </div>
                                                                <ul>
                                                                    <li><a href="#">Veniam quis nostrud</a>
                                                                    </li>
                                                                    <li><a href="#">Exercitation</a></li>
                                                                    <li><a href="#">Laboris nisi ut
                                                                        aliuip</a></li>
                                                                    <li><a href="#">Commodo conseat</a></li>
                                                                    <li><a href="#">Duis aute irure</a></li>
                                                                </ul>
                                                                <a className="tg-btnviewall" href="#">View
                                                                    All</a>
                                                            </li>
                                                        </ul>
                                                        <ul>
                                                            <li>
                                                                <figure><img src={img01}
                                                                             alt="image description"/></figure>
                                                                <div className="tg-textbox">
                                                                    <h3>More Than<span>12,0657,53</span>Books Collection
                                                                    </h3>
                                                                    <div className="tg-description">
                                                                        <p>Consectetur adipisicing elit sed doe eiusmod
                                                                            tempor incididunt laebore toloregna aliqua
                                                                            enim.</p>
                                                                    </div>
                                                                    <a className="tg-btn" href="#">view
                                                                        all</a>
                                                                </div>
                                                            </li>
                                                        </ul>
                                                    </div>
                                                    <div role="tabpanel" className="tab-pane" id="fooddrink">
                                                        <ul>
                                                            <li>
                                                                <div className="tg-linkstitle">
                                                                    <h2>History</h2>
                                                                </div>
                                                                <ul>
                                                                    <li><a href="#">Veniam quis nostrud</a>
                                                                    </li>
                                                                    <li><a href="#">Exercitation</a></li>
                                                                    <li><a href="#">Laboris nisi ut
                                                                        aliuip</a></li>
                                                                    <li><a href="#">Commodo conseat</a></li>
                                                                    <li><a href="#">Duis aute irure</a></li>
                                                                </ul>
                                                                <a className="tg-btnviewall" href="#">View
                                                                    All</a>
                                                            </li>
                                                            <li>
                                                                <div className="tg-linkstitle">
                                                                    <h2>Architecture</h2>
                                                                </div>
                                                                <ul>
                                                                    <li><a href="#">Tough As Nails</a></li>
                                                                    <li><a href="#">Pro Grease Monkey</a>
                                                                    </li>
                                                                    <li><a href="#">Building Memories</a>
                                                                    </li>
                                                                    <li><a href="#">Bulldozer Boyz</a></li>
                                                                    <li><a href="#">Build Or Leave On Us</a>
                                                                    </li>
                                                                </ul>
                                                                <a className="tg-btnviewall" href="#">View
                                                                    All</a>
                                                            </li>
                                                            <li>
                                                                <div className="tg-linkstitle">
                                                                    <h2>Art Forms</h2>
                                                                </div>
                                                                <ul>
                                                                    <li><a href="#">Consectetur
                                                                        adipisicing</a></li>
                                                                    <li><a href="#">Aelit sed do eiusmod</a>
                                                                    </li>
                                                                    <li><a href="#">Tempor incididunt
                                                                        labore</a></li>
                                                                    <li><a href="#">Dolore magna aliqua</a>
                                                                    </li>
                                                                    <li><a href="#">Ut enim ad minim</a>
                                                                    </li>
                                                                </ul>
                                                                <a className="tg-btnviewall" href="#">View
                                                                    All</a>
                                                            </li>
                                                        </ul>
                                                        <ul>
                                                            <li>
                                                                <figure><img src={img01}
                                                                             alt="image description"/></figure>
                                                                <div className="tg-textbox">
                                                                    <h3>More Than<span>12,0657,53</span>Books Collection
                                                                    </h3>
                                                                    <div className="tg-description">
                                                                        <p>Consectetur adipisicing elit sed doe eiusmod
                                                                            tempor incididunt laebore toloregna aliqua
                                                                            enim.</p>
                                                                    </div>
                                                                    <a className="tg-btn" href="#">view
                                                                        all</a>
                                                                </div>
                                                            </li>
                                                        </ul>
                                                    </div>
                                                    <div role="tabpanel" className="tab-pane" id="graphicanimemanga">
                                                        <ul>
                                                            <li>
                                                                <div className="tg-linkstitle">
                                                                    <h2>Architecture</h2>
                                                                </div>
                                                                <ul>
                                                                    <li><a href="#">Tough As Nails</a></li>
                                                                    <li><a href="#">Pro Grease Monkey</a>
                                                                    </li>
                                                                    <li><a href="#">Building Memories</a>
                                                                    </li>
                                                                    <li><a href="#">Bulldozer Boyz</a></li>
                                                                    <li><a href="#">Build Or Leave On Us</a>
                                                                    </li>
                                                                </ul>
                                                                <a className="tg-btnviewall" href="#">View
                                                                    All</a>
                                                            </li>
                                                            <li>
                                                                <div className="tg-linkstitle">
                                                                    <h2>Art Forms</h2>
                                                                </div>
                                                                <ul>
                                                                    <li><a href="#">Consectetur
                                                                        adipisicing</a></li>
                                                                    <li><a href="#">Aelit sed do eiusmod</a>
                                                                    </li>
                                                                    <li><a href="#">Tempor incididunt
                                                                        labore</a></li>
                                                                    <li><a href="#">Dolore magna aliqua</a>
                                                                    </li>
                                                                    <li><a href="#">Ut enim ad minim</a>
                                                                    </li>
                                                                </ul>
                                                                <a className="tg-btnviewall" href="#">View
                                                                    All</a>
                                                            </li>
                                                            <li>
                                                                <div className="tg-linkstitle">
                                                                    <h2>History</h2>
                                                                </div>
                                                                <ul>
                                                                    <li><a href="#">Veniam quis nostrud</a>
                                                                    </li>
                                                                    <li><a href="#">Exercitation</a></li>
                                                                    <li><a href="#">Laboris nisi ut
                                                                        aliuip</a></li>
                                                                    <li><a href="#">Commodo conseat</a></li>
                                                                    <li><a href="#">Duis aute irure</a></li>
                                                                </ul>
                                                                <a className="tg-btnviewall" href="#">View
                                                                    All</a>
                                                            </li>
                                                        </ul>
                                                        <ul>
                                                            <li>
                                                                <figure><img src={img01}
                                                                             alt="image description"/></figure>
                                                                <div className="tg-textbox">
                                                                    <h3>More Than<span>12,0657,53</span>Books Collection
                                                                    </h3>
                                                                    <div className="tg-description">
                                                                        <p>Consectetur adipisicing elit sed doe eiusmod
                                                                            tempor incididunt laebore toloregna aliqua
                                                                            enim.</p>
                                                                    </div>
                                                                    <a className="tg-btn" href="#">view
                                                                        all</a>
                                                                </div>
                                                            </li>
                                                        </ul>
                                                    </div>
                                                    <div role="tabpanel" className="tab-pane" id="sciencefiction">
                                                        <ul>
                                                            <li>
                                                                <div className="tg-linkstitle">
                                                                    <h2>History</h2>
                                                                </div>
                                                                <ul>
                                                                    <li><a href="#">Veniam quis nostrud</a>
                                                                    </li>
                                                                    <li><a href="#">Exercitation</a></li>
                                                                    <li><a href="#">Laboris nisi ut
                                                                        aliuip</a></li>
                                                                    <li><a href="#">Commodo conseat</a></li>
                                                                    <li><a href="#">Duis aute irure</a></li>
                                                                </ul>
                                                                <a className="tg-btnviewall" href="#">View
                                                                    All</a>
                                                            </li>
                                                            <li>
                                                                <div className="tg-linkstitle">
                                                                    <h2>Architecture</h2>
                                                                </div>
                                                                <ul>
                                                                    <li><a href="#">Tough As Nails</a></li>
                                                                    <li><a href="#">Pro Grease Monkey</a>
                                                                    </li>
                                                                    <li><a href="#">Building Memories</a>
                                                                    </li>
                                                                    <li><a href="#">Bulldozer Boyz</a></li>
                                                                    <li><a href="#">Build Or Leave On Us</a>
                                                                    </li>
                                                                </ul>
                                                                <a className="tg-btnviewall" href="#">View
                                                                    All</a>
                                                            </li>
                                                            <li>
                                                                <div className="tg-linkstitle">
                                                                    <h2>Art Forms</h2>
                                                                </div>
                                                                <ul>
                                                                    <li><a href="#">Consectetur
                                                                        adipisicing</a></li>
                                                                    <li><a href="#">Aelit sed do eiusmod</a>
                                                                    </li>
                                                                    <li><a href="#">Tempor incididunt
                                                                        labore</a></li>
                                                                    <li><a href="#">Dolore magna aliqua</a>
                                                                    </li>
                                                                    <li><a href="#">Ut enim ad minim</a>
                                                                    </li>
                                                                </ul>
                                                                <a className="tg-btnviewall" to="#">View
                                                                    All</a>
                                                            </li>
                                                        </ul>
                                                        <ul>
                                                            <li>
                                                                <figure><img src={img01}
                                                                             alt="image description"/></figure>
                                                                <div className="tg-textbox">
                                                                    <h3>More Than<span>12,0657,53</span>Books Collection
                                                                    </h3>
                                                                    <div className="tg-description">
                                                                        <p>Consectetur adipisicing elit sed doe eiusmod
                                                                            tempor incididunt laebore toloregna aliqua
                                                                            enim.</p>
                                                                    </div>
                                                                    <a className="tg-btn" to="#">view
                                                                        all</a>
                                                                </div>
                                                            </li>
                                                        </ul>
                                                    </div>
                                                </div>
                                            </div>
                                        </li>
                                        <li className="menu-item-has-children current-menu-item">
                                            <a href='#' onClick={e => e.preventDefault()}>Home</a>
                                            <ul className="sub-menu">
                                                <li className="current-menu-item"><a href="#">Home V one</a>
                                                </li>
                                                <li><a >Home V two</a></li>
                                                <li><a href="#">Home V three</a></li>
                                            </ul>
                                        </li>
                                        <li className="menu-item-has-children">
                                            <a to='#' onClick={e => e.preventDefault()}>Authors</a>
                                            <ul className="sub-menu">
                                                <li><a href="#">Authors</a></li>
                                                <li><a href="#">Author Detail</a></li>
                                            </ul>
                                        </li>
                                        <li><a href="#">Best Selling</a></li>
                                        <li><a href="#">Weekly Sale</a></li>
                                        <li className="menu-item-has-children">
                                            <a to='#' onClick={e => e.preventDefault()}>Latest News</a>
                                            <ul className="sub-menu">
                                                <li><a href="#">News List</a></li>
                                                <li><a>News Grid</a></li>
                                                <li><a>News Detail</a></li>
                                            </ul>
                                        </li>
                                        <li><a>Contact</a></li>
                                        <li className="menu-item-has-children current-menu-item">
                                            <a><i className="icon-menu"></i></a>
                                            <ul className="sub-menu">
                                                <li className="menu-item-has-children">
                                                    <a>Products</a>
                                                    <ul className="sub-menu">
                                                        <li><a>Products</a></li>
                                                        <li><a>Product Detail</a></li>
                                                    </ul>
                                                </li>
                                                <li><a>About Us</a></li>
                                                <li><a href="#">404 Error</a></li>
                                                <li><a href="#">Coming Soon</a></li>
                                            </ul>
                                        </li>
                                    </ul>
                                </div>
                            </nav>
                        </div>
                    </div>
                </div>
            </div>
        </header>

        </>
    )
}
 export default Header