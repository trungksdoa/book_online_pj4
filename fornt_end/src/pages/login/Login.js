import React, { useState } from "react";
import styles from './style.module.css';
import { AUTH } from  '../../services/server';
import { useNavigate } from 'react-router-dom';
import { TextField, Button, Grid, FormControlLabel, Checkbox } from '@material-ui/core';
import 'bootstrap/dist/css/bootstrap.min.css';

const Login = () => {
    const history = useNavigate();
    const[userData,setUserData]=useState({username:"",password:""});
    const[errorMessage,setErrorMessage]=useState({value:""});

    const handleInputChange = (e)=>{
        setUserData((prevState) => {
            return {
              ...prevState,
              [e.target.name]: e.target.value,
            };
          });

    };
    const handleSubmit = (e) => {
        e.preventDefault();
    
        //if username or password field is empty, return error message
        if (userData.username === "" || userData.password === "") {
          setErrorMessage((prevState) => ({
            value: "Empty username/password field",
          }));
    
        } else if (
          userData.username.toLowerCase() === "admin" &&
          userData.password === "123456"
        ) {
          //Signin Success
        //   localStorage.setItem("isAuthenticated", "true");
          AUTH.IS_AUTHENTICATED = true;
          history.push('/');
        //   window.location.pathname = "/";
        } else {
          //If credentials entered is invalid
          setErrorMessage((prevState) => ({ value: "Invalid username/password" }));
          return;
        }
      };
    return (
        <div >
            <form className={styles.bodyForm}>
                <h2>Account</h2>
              <br></br>
                <Grid container>
                    <Grid item xs={12} >
                        <TextField
                            
                            fullWidth
                            name="username"
                            label="Your Username"
                            autoComplete='off'
                            onChange={(e)=>handleInputChange(e)}
                            
                        />
                    </Grid>
                    <Grid item xs={12} className={styles.passWordIp}>
                        <TextField
                            
                            fullWidth
                            name="password"
                            type="text"
                            label="Your Password"
                            autoComplete="off"
                            onChange={(e)=>handleInputChange(e)}
                      
                        />
                    </Grid>
                    <Grid item xs={12} className={styles.Staysignedin}> 
                        <FormControlLabel
                            control={<Checkbox color="primary"   />}
                            label="Stay signed in"
                            labelPlacement="end"
                            />
                    </Grid>
                    <Grid item xs={12}>
                        <Button
                            fullWidth
                            variant="contained"
                            color="primary"
                            onClick={handleSubmit}
                          >
                              Login
                            
                        </Button>
                    </Grid>

                </Grid>
            </form>
        </div>
// https://github.com/ismamz/react-bootstrap-icons/blob/master/src/index.js
    )
}
export default Login