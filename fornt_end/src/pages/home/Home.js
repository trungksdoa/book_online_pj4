import React from "react";
import {useEffect, useState} from "react";
import styles from './style.module.css';
import employeeApi from "../../api/employeeApi";
import img02 from "../../images/img-02.png";
import imgBooks1 from "../../images/books/img-01.jpg";
import imgBooks2 from "../../images/books/img-02.jpg";
import imgBooks3 from "../../images/books/img-03.jpg";
import imgBooks4 from "../../images/books/img-04.jpg";
import imgBooks5 from "../../images/books/img-05.jpg";
import imgBooks6 from "../../images/books/img-06.jpg";
import parallax from "../../images/parallax/bgparallax-04.jpg";
import authorImg02 from "../../images/author/imag-02.jpg";
import blogImg03 from "../../images/blog/img-03.jpg";






function Home() {

    // const [show, setShow] = useState(false);
   
    // const handleClose = () => setShow(false);
    // const handleShow = () => setShow(true);
    const [employeeList, setemployeeList] = useState([]);
    useEffect(() => {
        const fetchEmployeeList = async () => {
            try {
                const response = await employeeApi.getAll();
                setemployeeList(response);
                console.log('List_Employee is ', response);

            } catch (error) {
                console.log('failed to fetch employee list', error);
            }
        }
        fetchEmployeeList();
    }, [])


    return (

<>

         <div>
            <h1 >Home Pages</h1>
            <table className={styles.table} >
            <thead>
            <tr>
                <th>Seq</th>
                <th>firstName</th>
                <th>lastName</th>
        
            </tr>
            </thead>
            <tbody>
            {employeeList.map((item, index) => {
               const {id, firstName, lastName} = item;
               return (<tr key={id}>
                   <td>{id}</td>
                   <td>{firstName}</td>
                   <td>{lastName}</td>
               </tr>);
            })}
            </tbody>
        </table>
        </div>
             <section className="tg-sectionspace tg-haslayout">

                <div className="container">
                    <div className="row">
                        <div className="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                            <div className="tg-sectionhead">
                                <h2><span>People’s Choice</span>Bestselling Books</h2>
                                <a className="tg-btn" href="javascript:void(0);">View All</a>
                            </div>
                        </div>
                        <div className="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                            <div id="tg-bestsellingbooksslider"
                                 className="tg-bestsellingbooksslider tg-bestsellingbooks owl-carousel owl-loaded owl-drag">
                                <div className="owl-stage-outer">
                                    <div className="owl-stage" style={{
                                        transform: 'translate3d(-1170px, 0px, 0px)',
                                        transition: 'all 0s ease 0s',
                                        width: '3705px'
                                    }}>
                                        <div className="owl-item cloned" style={{width: '165px', marginRight: '30px'}}>
                                            <div className="item">
                                                <div className="tg-postbook">
                                                    <figure className="tg-featureimg">
                                                        <div className="tg-bookimg">
                                                            <div className="tg-frontcover"><img
                                                                src={imgBooks1} alt="image description"/>
                                                            </div>
                                                            <div className="tg-backcover"><img
                                                                src={imgBooks1} alt="image description"/>
                                                            </div>
                                                        </div>
                                                        <a className="tg-btnaddtowishlist" href="javascript:void(0);">
                                                            <i className="icon-heart"/>
                                                            <span>add to wishlist</span>
                                                        </a>
                                                    </figure>
                                                    <div className="tg-postbookcontent">
                                                        <ul className="tg-bookscategories">
                                                            <li><a href="javascript:void(0);">Adventure</a></li>
                                                            <li><a href="javascript:void(0);">Fun</a></li>
                                                        </ul>
                                                        <div className="tg-themetagbox"><span
                                                            className="tg-themetag">sale</span></div>
                                                        <div className="tg-booktitle">
                                                            <h3><a href="javascript:void(0);">Drive Safely, No
                                                                Bumping</a></h3>
                                                        </div>
                                                        <span className="tg-bookwriter">By: <a
                                                            href="javascript:void(0);">Angela Gunning</a></span>
                                                        <span className="tg-stars"><span/></span>
                                                        <span className="tg-bookprice">
                        <ins>$25.18</ins>
                        <del>$27.20</del>
                        </span>
                                                        <a className="tg-btn tg-btnstyletwo" href="javascript:void(0);">
                                                            <i className="fa fa-shopping-basket"/>
                                                            <em>Add To Basket</em>
                                                        </a>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div className="owl-item cloned" style={{width: '165px', marginRight: '30px'}}>
                                            <div className="item">
                                                <div className="tg-postbook tg-notag">
                                                    <figure className="tg-featureimg">
                                                        <div className="tg-bookimg">
                                                            <div className="tg-frontcover"><img
                                                                src={imgBooks2} alt="image description"/>
                                                            </div>
                                                            <div className="tg-backcover"><img
                                                                src={imgBooks2} alt="image description"/>
                                                            </div>
                                                        </div>
                                                        <a className="tg-btnaddtowishlist" href="javascript:void(0);">
                                                            <i className="icon-heart"/>
                                                            <span>add to wishlist</span>
                                                        </a>
                                                    </figure>
                                                    <div className="tg-postbookcontent">
                                                        <ul className="tg-bookscategories">
                                                            <li><a href="javascript:void(0);">Adventure</a></li>
                                                            <li><a href="javascript:void(0);">Fun</a></li>
                                                        </ul>
                                                        <div className="tg-booktitle">
                                                            <h3><a href="javascript:void(0);">Let The Good Times Roll
                                                                Up</a></h3>
                                                        </div>
                                                        <span className="tg-bookwriter">By: <a
                                                            href="javascript:void(0);">Angela Gunning</a></span>
                                                        <span className="tg-stars"><span/></span>
                                                        <span className="tg-bookprice">
                        <ins>$25.18</ins>
                        <del>$27.20</del>
                        </span>
                                                        <a className="tg-btn tg-btnstyletwo" href="javascript:void(0);">
                                                            <i className="fa fa-shopping-basket"/>
                                                            <em>Add To Basket</em>
                                                        </a>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div className="owl-item cloned" style={{width: '165px', marginRight: '30px'}}>
                                            <div className="item">
                                                <div className="tg-postbook">
                                                    <figure className="tg-featureimg">
                                                        <div className="tg-bookimg">
                                                            <div className="tg-frontcover"><img
                                                                src={imgBooks2} alt="image description"/>
                                                            </div>
                                                            <div className="tg-backcover"><img
                                                                src={imgBooks2} alt="image description"/>
                                                            </div>
                                                        </div>
                                                        <a className="tg-btnaddtowishlist" href="javascript:void(0);">
                                                            <i className="icon-heart"/>
                                                            <span>add to wishlist</span>
                                                        </a>
                                                    </figure>
                                                    <div className="tg-postbookcontent">
                                                        <ul className="tg-bookscategories">
                                                            <li><a href="javascript:void(0);">Adventure</a></li>
                                                            <li><a href="javascript:void(0);">Fun</a></li>
                                                        </ul>
                                                        <div className="tg-themetagbox"><span
                                                            className="tg-themetag">sale</span></div>
                                                        <div className="tg-booktitle">
                                                            <h3><a href="javascript:void(0);">Our State Fair Is A Great
                                                                State Fair</a></h3>
                                                        </div>
                                                        <span className="tg-bookwriter">By: <a
                                                            href="javascript:void(0);">Angela Gunning</a></span>
                                                        <span className="tg-stars"><span/></span>
                                                        <span className="tg-bookprice">
                        <ins>$25.18</ins>
                        <del>$27.20</del>
                        </span>
                                                        <a className="tg-btn tg-btnstyletwo" href="javascript:void(0);">
                                                            <i className="fa fa-shopping-basket"/>
                                                            <em>Add To Basket</em>
                                                        </a>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div className="owl-item cloned" style={{width: '165px', marginRight: '30px'}}>
                                            <div className="item">
                                                <div className="tg-postbook tg-notag">
                                                    <figure className="tg-featureimg">
                                                        <div className="tg-bookimg">
                                                            <div className="tg-frontcover"><img
                                                                src={imgBooks2} alt="image description"/>
                                                            </div>
                                                            <div className="tg-backcover"><img
                                                                src={imgBooks2} alt="image description"/>
                                                            </div>
                                                        </div>
                                                        <a className="tg-btnaddtowishlist" href="javascript:void(0);">
                                                            <i className="icon-heart"/>
                                                            <span>add to wishlist</span>
                                                        </a>
                                                    </figure>
                                                    <div className="tg-postbookcontent">
                                                        <ul className="tg-bookscategories">
                                                            <li><a href="javascript:void(0);">Adventure</a></li>
                                                            <li><a href="javascript:void(0);">Fun</a></li>
                                                        </ul>
                                                        <div className="tg-booktitle">
                                                            <h3><a href="javascript:void(0);">Put The Petal To The
                                                                Metal</a></h3>
                                                        </div>
                                                        <span className="tg-bookwriter">By: <a
                                                            href="javascript:void(0);">Angela Gunning</a></span>
                                                        <span className="tg-stars"><span/></span>
                                                        <span className="tg-bookprice">
                        <ins>$25.18</ins>
                        <del>$27.20</del>
                        </span>
                                                        <a className="tg-btn tg-btnstyletwo" href="javascript:void(0);">
                                                            <i className="fa fa-shopping-basket"/>
                                                            <em>Add To Basket</em>
                                                        </a>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div className="owl-item cloned" style={{width: '165px', marginRight: '30px'}}>
                                            <div className="item">
                                                <div className="tg-postbook">
                                                    <figure className="tg-featureimg">
                                                        <div className="tg-bookimg">
                                                            <div className="tg-frontcover"><img
                                                                src={imgBooks5} alt="image description"/>
                                                            </div>
                                                            <div className="tg-backcover"><img
                                                                src={imgBooks5} alt="image description"/>
                                                            </div>
                                                        </div>
                                                        <a className="tg-btnaddtowishlist" href="javascript:void(0);">
                                                            <i className="icon-heart"/>
                                                            <span>add to wishlist</span>
                                                        </a>
                                                    </figure>
                                                    <div className="tg-postbookcontent">
                                                        <ul className="tg-bookscategories">
                                                            <li><a href="javascript:void(0);">Adventure</a></li>
                                                            <li><a href="javascript:void(0);">Fun</a></li>
                                                        </ul>
                                                        <div className="tg-themetagbox"><span
                                                            className="tg-themetag">sale</span></div>
                                                        <div className="tg-booktitle">
                                                            <h3><a href="javascript:void(0);">Help Me Find My
                                                                Stomach</a></h3>
                                                        </div>
                                                        <span className="tg-bookwriter">By: <a
                                                            href="javascript:void(0);">Angela Gunning</a></span>
                                                        <span className="tg-stars"><span/></span>
                                                        <span className="tg-bookprice">
                        <ins>$25.18</ins>
                        <del>$27.20</del>
                        </span>
                                                        <a className="tg-btn tg-btnstyletwo" href="javascript:void(0);">
                                                            <i className="fa fa-shopping-basket"/>
                                                            <em>Add To Basket</em>
                                                        </a>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div className="owl-item cloned" style={{width: '165px', marginRight: '30px'}}>
                                            <div className="item">
                                                <div className="tg-postbook tg-notag">
                                                    <figure className="tg-featureimg">
                                                        <div className="tg-bookimg">
                                                            <div className="tg-frontcover"><img
                                                                src={imgBooks2} alt="image description"/>
                                                            </div>
                                                            <div className="tg-backcover"><img
                                                                src={imgBooks2} alt="image description"/>
                                                            </div>
                                                        </div>
                                                        <a className="tg-btnaddtowishlist" href="javascript:void(0);">
                                                            <i className="icon-heart"/>
                                                            <span>add to wishlist</span>
                                                        </a>
                                                    </figure>
                                                    <div className="tg-postbookcontent">
                                                        <ul className="tg-bookscategories">
                                                            <li><a href="javascript:void(0);">Adventure</a></li>
                                                            <li><a href="javascript:void(0);">Fun</a></li>
                                                        </ul>
                                                        <div className="tg-booktitle">
                                                            <h3><a href="javascript:void(0);">Let The Good Times Roll
                                                                Up</a></h3>
                                                        </div>
                                                        <span className="tg-bookwriter">By: <a
                                                            href="javascript:void(0);">Angela Gunning</a></span>
                                                        <span className="tg-stars"><span/></span>
                                                        <span className="tg-bookprice">
                        <ins>$25.18</ins>
                        <del>$27.20</del>
                        </span>
                                                        <a className="tg-btn tg-btnstyletwo" href="javascript:void(0);">
                                                            <i className="fa fa-shopping-basket"/>
                                                            <em>Add To Basket</em>
                                                        </a>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div className="owl-item active" style={{width: '165px', marginRight: '30px'}}>
                                            <div className="item">
                                                <div className="tg-postbook">
                                                    <figure className="tg-featureimg">
                                                        <div className="tg-bookimg">
                                                            <div className="tg-frontcover"><img
                                                                src={imgBooks1} alt="image description"/>
                                                            </div>
                                                            <div className="tg-backcover"><img
                                                                src={imgBooks1} alt="image description"/>
                                                            </div>
                                                        </div>
                                                        <a className="tg-btnaddtowishlist" href="javascript:void(0);">
                                                            <i className="icon-heart"/>
                                                            <span>add to wishlist</span>
                                                        </a>
                                                    </figure>
                                                    <div className="tg-postbookcontent">
                                                        <ul className="tg-bookscategories">
                                                            <li><a href="javascript:void(0);">Adventure</a></li>
                                                            <li><a href="javascript:void(0);">Fun</a></li>
                                                        </ul>
                                                        <div className="tg-themetagbox"><span
                                                            className="tg-themetag">sale</span></div>
                                                        <div className="tg-booktitle">
                                                            <h3><a href="javascript:void(0);">Help Me Find My
                                                                Stomach</a></h3>
                                                        </div>
                                                        <span className="tg-bookwriter">By: <a
                                                            href="javascript:void(0);">Angela Gunning</a></span>
                                                        <span className="tg-stars"><span/></span>
                                                        <span className="tg-bookprice">
                        <ins>$25.18</ins>
                        <del>$27.20</del>
                        </span>
                                                        <a className="tg-btn tg-btnstyletwo" href="javascript:void(0);">
                                                            <i className="fa fa-shopping-basket"/>
                                                            <em>Add To Basket</em>
                                                        </a>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div className="owl-item active" style={{width: '165px', marginRight: '30px'}}>
                                            <div className="item">
                                                <div className="tg-postbook">
                                                    <figure className="tg-featureimg">
                                                        <div className="tg-bookimg">
                                                            <div className="tg-frontcover"><img
                                                                src={imgBooks2} alt="image description"/>
                                                            </div>
                                                            <div className="tg-backcover"><img
                                                                src={imgBooks2} alt="image description"/>
                                                            </div>
                                                        </div>
                                                        <a className="tg-btnaddtowishlist" href="javascript:void(0);">
                                                            <i className="icon-heart"/>
                                                            <span>add to wishlist</span>
                                                        </a>
                                                    </figure>
                                                    <div className="tg-postbookcontent">
                                                        <ul className="tg-bookscategories">
                                                            <li><a href="javascript:void(0);">Adventure</a></li>
                                                            <li><a href="javascript:void(0);">Fun</a></li>
                                                        </ul>
                                                        <div className="tg-themetagbox"><span
                                                            className="tg-themetag">sale</span></div>
                                                        <div className="tg-booktitle">
                                                            <h3><a href="javascript:void(0);">Drive Safely, No
                                                                Bumping</a></h3>
                                                        </div>
                                                        <span className="tg-bookwriter">By: <a
                                                            href="javascript:void(0);">Angela Gunning</a></span>
                                                        <span className="tg-stars"><span/></span>
                                                        <span className="tg-bookprice">
                        <ins>$25.18</ins>
                        <del>$27.20</del>
                        </span>
                                                        <a className="tg-btn tg-btnstyletwo" href="javascript:void(0);">
                                                            <i className="fa fa-shopping-basket"/>
                                                            <em>Add To Basket</em>
                                                        </a>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div className="owl-item active" style={{width: '165px', marginRight: '30px'}}>
                                            <div className="item">
                                                <div className="tg-postbook tg-notag">
                                                    <figure className="tg-featureimg">
                                                        <div className="tg-bookimg">
                                                            <div className="tg-frontcover"><img
                                                                src={imgBooks3} alt="image description"/>
                                                            </div>
                                                            <div className="tg-backcover"><img
                                                                src={imgBooks3} alt="image description"/>
                                                            </div>
                                                        </div>
                                                        <a className="tg-btnaddtowishlist" href="javascript:void(0);">
                                                            <i className="icon-heart"/>
                                                            <span>add to wishlist</span>
                                                        </a>
                                                    </figure>
                                                    <div className="tg-postbookcontent">
                                                        <ul className="tg-bookscategories">
                                                            <li><a href="javascript:void(0);">Adventure</a></li>
                                                            <li><a href="javascript:void(0);">Fun</a></li>
                                                        </ul>
                                                        <div className="tg-booktitle">
                                                            <h3><a href="javascript:void(0);">Let The Good Times Roll
                                                                Up</a></h3>
                                                        </div>
                                                        <span className="tg-bookwriter">By: <a
                                                            href="javascript:void(0);">Angela Gunning</a></span>
                                                        <span className="tg-stars"><span/></span>
                                                        <span className="tg-bookprice">
                        <ins>$25.18</ins>
                        <del>$27.20</del>
                        </span>
                                                        <a className="tg-btn tg-btnstyletwo" href="javascript:void(0);">
                                                            <i className="fa fa-shopping-basket"/>
                                                            <em>Add To Basket</em>
                                                        </a>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div className="owl-item active" style={{width: '165px', marginRight: '30px'}}>
                                            <div className="item">
                                                <div className="tg-postbook">
                                                    <figure className="tg-featureimg">
                                                        <div className="tg-bookimg">
                                                            <div className="tg-frontcover"><img
                                                                src={imgBooks4} alt="image description"/>
                                                            </div>
                                                            <div className="tg-backcover"><img
                                                                src={imgBooks4} alt="image description"/>
                                                            </div>
                                                        </div>
                                                        <a className="tg-btnaddtowishlist" href="javascript:void(0);">
                                                            <i className="icon-heart"/>
                                                            <span>add to wishlist</span>
                                                        </a>
                                                    </figure>
                                                    <div className="tg-postbookcontent">
                                                        <ul className="tg-bookscategories">
                                                            <li><a href="javascript:void(0);">Adventure</a></li>
                                                            <li><a href="javascript:void(0);">Fun</a></li>
                                                        </ul>
                                                        <div className="tg-themetagbox"><span
                                                            className="tg-themetag">sale</span></div>
                                                        <div className="tg-booktitle">
                                                            <h3><a href="javascript:void(0);">Our State Fair Is A Great
                                                                State Fair</a></h3>
                                                        </div>
                                                        <span className="tg-bookwriter">By: <a
                                                            href="javascript:void(0);">Angela Gunning</a></span>
                                                        <span className="tg-stars"><span/></span>
                                                        <span className="tg-bookprice">
                        <ins>$25.18</ins>
                        <del>$27.20</del>
                        </span>
                                                        <a className="tg-btn tg-btnstyletwo" href="javascript:void(0);">
                                                            <i className="fa fa-shopping-basket"/>
                                                            <em>Add To Basket</em>
                                                        </a>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div className="owl-item active" style={{width: '165px', marginRight: '30px'}}>
                                            <div className="item">
                                                <div className="tg-postbook tg-notag">
                                                    <figure className="tg-featureimg">
                                                        <div className="tg-bookimg">
                                                            <div className="tg-frontcover"><img
                                                                src={imgBooks5} alt="image description"/>
                                                            </div>
                                                            <div className="tg-backcover"><img
                                                                src={imgBooks5} alt="image description"/>
                                                            </div>
                                                        </div>
                                                        <a className="tg-btnaddtowishlist" href="javascript:void(0);">
                                                            <i className="icon-heart"/>
                                                            <span>add to wishlist</span>
                                                        </a>
                                                    </figure>
                                                    <div className="tg-postbookcontent">
                                                        <ul className="tg-bookscategories">
                                                            <li><a href="javascript:void(0);">Adventure</a></li>
                                                            <li><a href="javascript:void(0);">Fun</a></li>
                                                        </ul>
                                                        <div className="tg-booktitle">
                                                            <h3><a href="javascript:void(0);">Put The Petal To The
                                                                Metal</a></h3>
                                                        </div>
                                                        <span className="tg-bookwriter">By: <a
                                                            href="javascript:void(0);">Angela Gunning</a></span>
                                                        <span className="tg-stars"><span/></span>
                                                        <span className="tg-bookprice">
                        <ins>$25.18</ins>
                        <del>$27.20</del>
                        </span>
                                                        <a className="tg-btn tg-btnstyletwo" href="javascript:void(0);">
                                                            <i className="fa fa-shopping-basket"/>
                                                            <em>Add To Basket</em>
                                                        </a>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div className="owl-item active" style={{width: '165px', marginRight: '30px'}}>
                                            <div className="item">
                                                <div className="tg-postbook">
                                                    <figure className="tg-featureimg">
                                                        <div className="tg-bookimg">
                                                            <div className="tg-frontcover"><img
                                                                src={imgBooks6} alt="image description"/>
                                                            </div>
                                                            <div className="tg-backcover"><img
                                                                src={imgBooks6} alt="image description"/>
                                                            </div>
                                                        </div>
                                                        <a className="tg-btnaddtowishlist" href="javascript:void(0);">
                                                            <i className="icon-heart"/>
                                                            <span>add to wishlist</span>
                                                        </a>
                                                    </figure>
                                                    <div className="tg-postbookcontent">
                                                        <ul className="tg-bookscategories">
                                                            <li><a href="javascript:void(0);">Adventure</a></li>
                                                            <li><a href="javascript:void(0);">Fun</a></li>
                                                        </ul>
                                                        <div className="tg-themetagbox"><span
                                                            className="tg-themetag">sale</span></div>
                                                        <div className="tg-booktitle">
                                                            <h3><a href="javascript:void(0);">Help Me Find My
                                                                Stomach</a></h3>
                                                        </div>
                                                        <span className="tg-bookwriter">By: <a
                                                            href="javascript:void(0);">Angela Gunning</a></span>
                                                        <span className="tg-stars"><span/></span>
                                                        <span className="tg-bookprice">
                        <ins>$25.18</ins>
                        <del>$27.20</del>
                        </span>
                                                        <a className="tg-btn tg-btnstyletwo" href="javascript:void(0);">
                                                            <i className="fa fa-shopping-basket"/>
                                                            <em>Add To Basket</em>
                                                        </a>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>







                                    </div>
                                </div>
                                <div className="owl-nav">
                                    <div className="owl-prev tg-btnround tg-btnprev">
                                        <i className="icon-chevron-left"/>
                                    </div>
                                    <div className="owl-next tg-btnround tg-btnnext">
                                        <i className="icon-chevron-right"/>
                                    </div>
                                </div>
                                <div className="owl-dots">
                                    <div className="owl-dot active">
                                        <span/>
                                    </div>
                                    <div className="owl-dot">
                                        <span/>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
            </section> 
   
         <section className="tg-bglight tg-haslayout">
        <div className="container">
          <div className="row">
            <div className="tg-featureditm">
              <div className="col-xs-12 col-sm-12 col-md-4 col-lg-4 hidden-sm hidden-xs">
                <figure><img src={img02} alt="image description" /></figure>
              </div>
              <div className="col-xs-12 col-sm-12 col-md-8 col-lg-8">
                <div className="tg-featureditmcontent">
                  <div className="tg-themetagbox"><span className="tg-themetag">featured</span></div>
                  <div className="tg-booktitle">
                    <h3><a href="javascript:void(0);">Things To Know About Green Flat Design</a></h3>
                  </div>
                  <span className="tg-bookwriter">By: <a href="javascript:void(0);">Farrah Whisenhunt</a></span>
                  <span className="tg-stars"><span /></span>
                  <div className="tg-priceandbtn">
                    <span className="tg-bookprice">
                      <ins>$23.18</ins>
                      <del>$30.20</del>
                    </span>
                    <a className="tg-btn tg-btnstyletwo tg-active" href="javascript:void(0);">
                      <i className="fa fa-shopping-basket" />
                      <em>Add To Basket</em>
                    </a>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
             </section>

            <section className="tg-sectionspace tg-haslayout">
                <div className="container">
                    <div className="row">
                        <div className="tg-newrelease">
                            <div className="col-xs-12 col-sm-12 col-md-6 col-lg-6">
                                <div className="tg-sectionhead">
                                    <h2><span>Taste The New Spice</span>New Release Books</h2>
                                </div>
                                <div className="tg-description">
                                    <p>Consectetur adipisicing elit sed do eiusmod tempor incididunt labore toloregna
                                        aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamcoiars nisiuip
                                        commodo consequat aute irure dolor in aprehenderit aveli esseati cillum dolor
                                        fugiat nulla pariatur cepteur sint occaecat cupidatat.</p>
                                </div>
                                <div className="tg-btns">
                                    <a className="tg-btn tg-active" href="javascript:void(0);">View All</a>
                                    <a className="tg-btn" href="javascript:void(0);">Read More</a>
                                </div>
                            </div>
                            <div className="col-xs-12 col-sm-12 col-md-6 col-lg-6">
                                <div className="row">
                                    <div className="tg-newreleasebooks">
                                        <div className="col-xs-4 col-sm-4 col-md-6 col-lg-4">
                                            <div className="tg-postbook">
                                                <figure className="tg-featureimg">
                                                    <div className="tg-bookimg">
                                                        <div className="tg-frontcover"><img
                                                            src={imgBooks6} alt="image description"/>
                                                        </div>
                                                        <div className="tg-backcover"><img src={imgBooks6}
                                                                                           alt="image description"/>
                                                        </div>
                                                    </div>
                                                    <a className="tg-btnaddtowishlist" href="javascript:void(0);">
                                                        <i className="icon-heart"/>
                                                        <span>add to wishlist</span>
                                                    </a>
                                                </figure>
                                                <div className="tg-postbookcontent">
                                                    <ul className="tg-bookscategories">
                                                        <li><a href="javascript:void(0);">Adventure</a></li>
                                                        <li><a href="javascript:void(0);">Fun</a></li>
                                                    </ul>
                                                    <div className="tg-booktitle">
                                                        <h3><a href="javascript:void(0);">Help Me Find My Stomach</a>
                                                        </h3>
                                                    </div>
                                                    <span className="tg-bookwriter">By: <a href="javascript:void(0);">Kathrine Culbertson</a></span>
                                                    <span className="tg-stars"><span/></span>
                                                </div>
                                            </div>
                                        </div>
                                        <div className="col-xs-4 col-sm-4 col-md-6 col-lg-4">
                                            <div className="tg-postbook">
                                                <figure className="tg-featureimg">
                                                    <div className="tg-bookimg">
                                                        <div className="tg-frontcover"><img
                                                            src={imgBooks5} alt="image description"/>
                                                        </div>
                                                        <div className="tg-backcover"><img src={imgBooks5}
                                                                                           alt="image description"/>
                                                        </div>
                                                    </div>
                                                    <a className="tg-btnaddtowishlist" href="javascript:void(0);">
                                                        <i className="icon-heart"/>
                                                        <span>add to wishlist</span>
                                                    </a>
                                                </figure>
                                                <div className="tg-postbookcontent">
                                                    <ul className="tg-bookscategories">
                                                        <li><a href="javascript:void(0);">Adventure</a></li>
                                                        <li><a href="javascript:void(0);">Fun</a></li>
                                                    </ul>
                                                    <div className="tg-booktitle">
                                                        <h3><a href="javascript:void(0);">Drive Safely, No Bumping</a>
                                                        </h3>
                                                    </div>
                                                    <span className="tg-bookwriter">By: <a href="javascript:void(0);">Sunshine Orlando</a></span>
                                                    <span className="tg-stars"><span/></span>
                                                </div>
                                            </div>
                                        </div>
                                        <div className="col-xs-4 col-sm-4 col-md-3 col-lg-4 hidden-md">
                                            <div className="tg-postbook">
                                                <figure className="tg-featureimg">
                                                    <div className="tg-bookimg">
                                                        <div className="tg-frontcover"><img
                                                            src={imgBooks4} alt="image description"/>
                                                        </div>
                                                        <div className="tg-backcover"><img src={imgBooks4}
                                                                                           alt="image description"/>
                                                        </div>
                                                    </div>
                                                    <a className="tg-btnaddtowishlist" href="javascript:void(0);">
                                                        <i className="icon-heart"/>
                                                        <span>add to wishlist</span>
                                                    </a>
                                                </figure>
                                                <div className="tg-postbookcontent">
                                                    <ul className="tg-bookscategories">
                                                        <li><a href="javascript:void(0);">Adventure</a></li>
                                                        <li><a href="javascript:void(0);">Fun</a></li>
                                                    </ul>
                                                    <div className="tg-booktitle">
                                                        <h3><a href="javascript:void(0);">Let The Good Times Roll Up</a>
                                                        </h3>
                                                    </div>
                                                    <span className="tg-bookwriter">By: <a href="javascript:void(0);">Elisabeth Ronning</a></span>
                                                    <span className="tg-stars"><span/></span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section> 

            <section className="tg-parallax tg-bgcollectioncount tg-haslayout" data-z-index={-100} data-appear-top-offset={600} data-parallax="scroll" data-image-src={parallax}>
        <div className="tg-sectionspace tg-collectioncount tg-haslayout">
          <div className="container">
            <div className="row">
              <div id="tg-collectioncounters" className="tg-collectioncounters">
                <div className="tg-collectioncounter tg-drama">
                  <div className="tg-collectioncountericon">
                    <i className="icon-bubble" />
                  </div>
                  <div className="tg-titlepluscounter">
                    <h2>Drama</h2>
                    <h3 data-from={0} data-to={6179213} data-speed={8000} data-refresh-interval={50}>6,179,213</h3>
                  </div>
                </div>
                <div className="tg-collectioncounter tg-horror">
                  <div className="tg-collectioncountericon">
                    <i className="icon-heart-pulse" />
                  </div>
                  <div className="tg-titlepluscounter">
                    <h2>Horror</h2>
                    <h3 data-from={0} data-to={3121242} data-speed={8000} data-refresh-interval={50}>3,121,242</h3>
                  </div>
                </div>
                <div className="tg-collectioncounter tg-romance">
                  <div className="tg-collectioncountericon">
                    <i className="icon-heart" />
                  </div>
                  <div className="tg-titlepluscounter">
                    <h2>Romance</h2>
                    <h3 data-from={0} data-to={2101012} data-speed={8000} data-refresh-interval={50}>2,101,012</h3>
                  </div>
                </div>
                <div className="tg-collectioncounter tg-fashion">
                  <div className="tg-collectioncountericon">
                    <i className="icon-star" />
                  </div>
                  <div className="tg-titlepluscounter">
                    <h2>Fashion</h2>
                    <h3 data-from={0} data-to={1158245} data-speed={8000} data-refresh-interval={50}>1,158,245</h3>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </section>

           <section className="tg-parallax tg-bgtestimonials tg-haslayout" data-z-index={-100}
                     data-appear-top-offset={600} data-parallax="scroll"
                     data-image-src="images/parallax/bgparallax-05.jpg">
                <div className="tg-sectionspace tg-haslayout">
                    <div className="container">
                        <div className="row">
                            <div className="col-xs-12 col-sm-12 col-md-12 col-lg-8 col-lg-push-2">
                                <div id="tg-testimonialsslider"
                                     className="tg-testimonialsslider tg-testimonials owl-carousel owl-loaded owl-drag">
                                    <div className="owl-stage-outer">
                                        <div className="owl-stage" style={{
                                            transform: 'translate3d(-1500px, 0px, 0px)',
                                            transition: 'all 0s ease 0s',
                                            width: '5250px'
                                        }}>
                                            <div className="owl-item cloned" style={{width: '750px'}}>
                                                <div className="item tg-testimonial">
                                                    <figure><img src={authorImg02}
                                                                 alt="image description"/></figure>
                                                    <blockquote><q>Consectetur adipisicing elit sed do eiusmod tempor
                                                        incididunt ut labore tolore magna aliqua enim ad minim veniam,
                                                        quis nostrud exercitation ullamcoiars nisi ut aliquip
                                                        commodo.</q></blockquote>
                                                    <div className="tg-testimonialauthor">
                                                        <h3>Holli Fenstermacher</h3>
                                                        <span>Manager @ CIFP</span>
                                                    </div>
                                                </div>
                                            </div>
                                            <div className="owl-item cloned" style={{width: '750px'}}>
                                                <div className="item tg-testimonial">
                                                    <figure><img src={authorImg02}
                                                                 alt="image description"/></figure>
                                                    <blockquote><q>Consectetur adipisicing elit sed do eiusmod tempor
                                                        incididunt ut labore tolore magna aliqua enim ad minim veniam,
                                                        quis nostrud exercitation ullamcoiars nisi ut aliquip
                                                        commodo.</q></blockquote>
                                                    <div className="tg-testimonialauthor">
                                                        <h3>Holli Fenstermacher</h3>
                                                        <span>Manager @ CIFP</span>
                                                    </div>
                                                </div>
                                            </div>
                                            <div className="owl-item active" style={{width: '750px'}}>
                                                <div className="item tg-testimonial">
                                                    <figure><img src={authorImg02}
                                                                 alt="image description"/></figure>
                                                    <blockquote><q>Consectetur adipisicing elit sed do eiusmod tempor
                                                        incididunt ut labore tolore magna aliqua enim ad minim veniam,
                                                        quis nostrud exercitation ullamcoiars nisi ut aliquip
                                                        commodo.</q></blockquote>
                                                    <div className="tg-testimonialauthor">
                                                        <h3>Holli Fenstermacher</h3>
                                                        <span>Manager @ CIFP</span>
                                                    </div>
                                                </div>
                                            </div>
                                            <div className="owl-item" style={{width: '750px'}}>
                                                <div className="item tg-testimonial">
                                                    <figure><img src={authorImg02}
                                                                 alt="image description"/></figure>
                                                    <blockquote><q>Consectetur adipisicing elit sed do eiusmod tempor
                                                        incididunt ut labore tolore magna aliqua enim ad minim veniam,
                                                        quis nostrud exercitation ullamcoiars nisi ut aliquip
                                                        commodo.</q></blockquote>
                                                    <div className="tg-testimonialauthor">
                                                        <h3>Holli Fenstermacher</h3>
                                                        <span>Manager @ CIFP</span>
                                                    </div>
                                                </div>
                                            </div>
                                            <div className="owl-item" style={{width: '750px'}}>
                                                <div className="item tg-testimonial">
                                                    <figure><img src={authorImg02}
                                                                 alt="image description"/></figure>
                                                    <blockquote><q>Consectetur adipisicing elit sed do eiusmod tempor
                                                        incididunt ut labore tolore magna aliqua enim ad minim veniam,
                                                        quis nostrud exercitation ullamcoiars nisi ut aliquip
                                                        commodo.</q></blockquote>
                                                    <div className="tg-testimonialauthor">
                                                        <h3>Holli Fenstermacher</h3>
                                                        <span>Manager @ CIFP</span>
                                                    </div>
                                                </div>
                                            </div>
                                            <div className="owl-item cloned" style={{width: '750px'}}>
                                                <div className="item tg-testimonial">
                                                    <figure><img src={authorImg02}
                                                                 alt="image description"/></figure>
                                                    <blockquote><q>Consectetur adipisicing elit sed do eiusmod tempor
                                                        incididunt ut labore tolore magna aliqua enim ad minim veniam,
                                                        quis nostrud exercitation ullamcoiars nisi ut aliquip
                                                        commodo.</q></blockquote>
                                                    <div className="tg-testimonialauthor">
                                                        <h3>Holli Fenstermacher</h3>
                                                        <span>Manager @ CIFP</span>
                                                    </div>
                                                </div>
                                            </div>
                                            <div className="owl-item cloned" style={{width: '750px'}}>
                                                <div className="item tg-testimonial">
                                                    <figure><img src={authorImg02}
                                                                 alt="image description"/></figure>
                                                    <blockquote><q>Consectetur adipisicing elit sed do eiusmod tempor
                                                        incididunt ut labore tolore magna aliqua enim ad minim veniam,
                                                        quis nostrud exercitation ullamcoiars nisi ut aliquip
                                                        commodo.</q></blockquote>
                                                    <div className="tg-testimonialauthor">
                                                        <h3>Holli Fenstermacher</h3>
                                                        <span>Manager @ CIFP</span>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div className="owl-nav">
                                        <div className="owl-prev tg-btnround tg-btnprev"><i
                                            className="icon-chevron-left"/></div>
                                        <div className="owl-next tg-btnround tg-btnnext"><i
                                            className="icon-chevron-right"/></div>
                                    </div>
                                    <div className="owl-dots">
                                        <div className="owl-dot active"><span/></div>
                                        <div className="owl-dot"><span/></div>
                                        <div className="owl-dot"><span/></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section> 

            <section className="tg-sectionspace tg-haslayout">
                <div className="container">
                    <div className="row">
                        <div className="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                            <div className="tg-sectionhead">
                                <h2><span>Latest News &amp; Articles</span>What's Hot in The News</h2>
                                <a className="tg-btn" href="javascript:void(0);">View All</a>
                            </div>
                        </div>
                        <div id="tg-postslider" className="tg-postslider tg-blogpost owl-carousel owl-loaded owl-drag">
                            <div className="owl-stage-outer">
                                <div className="owl-stage" style={{
                                    transform: 'translate3d(-1170px, 0px, 0px)',
                                    transition: 'all 0s ease 0s',
                                    width: '4095px'
                                }}>
                                    <div className="owl-item cloned" style={{width: '292.5px'}}>
                                        <article className="item tg-post">
                                            <figure><a href="javascript:void(0);"><img src={blogImg03}
                                                                                       alt="image description"/></a>
                                            </figure>
                                            <div className="tg-postcontent">
                                                <ul className="tg-bookscategories">
                                                    <li><a href="javascript:void(0);">Adventure</a></li>
                                                    <li><a href="javascript:void(0);">Fun</a></li>
                                                </ul>
                                                <div className="tg-themetagbox"><span
                                                    className="tg-themetag">featured</span></div>
                                                <div className="tg-posttitle">
                                                    <h3><a href="javascript:void(0);">Why Walk When You Can Climb?</a>
                                                    </h3>
                                                </div>
                                                <span className="tg-bookwriter">By: <a href="javascript:void(0);">Kathrine Culbertson</a></span>
                                                <ul className="tg-postmetadata">
                                                    <li><a href="javascript:void(0);"><i
                                                        className="fa fa-comment-o"/><i>21,415 Comments</i></a></li>
                                                    <li><a href="javascript:void(0);"><i className="fa fa-eye"/><i>24,565
                                                        Views</i></a></li>
                                                </ul>
                                            </div>
                                        </article>
                                    </div>
                                    <div className="owl-item cloned" style={{width: '292.5px'}}>
                                        <article className="item tg-post">
                                            <figure><a href="javascript:void(0);"><img src={blogImg03}
                                                                                       alt="image description"/></a>
                                            </figure>
                                            <div className="tg-postcontent">
                                                <ul className="tg-bookscategories">
                                                    <li><a href="javascript:void(0);">Adventure</a></li>
                                                    <li><a href="javascript:void(0);">Fun</a></li>
                                                </ul>
                                                <div className="tg-themetagbox"><span
                                                    className="tg-themetag">featured</span></div>
                                                <div className="tg-posttitle">
                                                    <h3><a href="javascript:void(0);">Dance Like Nobody’s Watching</a>
                                                    </h3>
                                                </div>
                                                <span className="tg-bookwriter">By: <a href="javascript:void(0);">Kathrine Culbertson</a></span>
                                                <ul className="tg-postmetadata">
                                                    <li><a href="javascript:void(0);"><i
                                                        className="fa fa-comment-o"/><i>21,415 Comments</i></a></li>
                                                    <li><a href="javascript:void(0);"><i className="fa fa-eye"/><i>24,565
                                                        Views</i></a></li>
                                                </ul>
                                            </div>
                                        </article>
                                    </div>
                                    <div className="owl-item cloned" style={{width: '292.5px'}}>
                                        <article className="item tg-post">
                                            <figure><a href="javascript:void(0);"><img src={blogImg03}
                                                                                       alt="image description"/></a>
                                            </figure>
                                            <div className="tg-postcontent">
                                                <ul className="tg-bookscategories">
                                                    <li><a href="javascript:void(0);">Adventure</a></li>
                                                    <li><a href="javascript:void(0);">Fun</a></li>
                                                </ul>
                                                <div className="tg-themetagbox"><span
                                                    className="tg-themetag">featured</span></div>
                                                <div className="tg-posttitle">
                                                    <h3><a href="javascript:void(0);">All She Wants To Do Is Dance</a>
                                                    </h3>
                                                </div>
                                                <span className="tg-bookwriter">By: <a href="javascript:void(0);">Kathrine Culbertson</a></span>
                                                <ul className="tg-postmetadata">
                                                    <li><a href="javascript:void(0);"><i
                                                        className="fa fa-comment-o"/><i>21,415 Comments</i></a></li>
                                                    <li><a href="javascript:void(0);"><i className="fa fa-eye"/><i>24,565
                                                        Views</i></a></li>
                                                </ul>
                                            </div>
                                        </article>
                                    </div>
                                    <div className="owl-item cloned" style={{width: '292.5px'}}>
                                        <article className="item tg-post">
                                            <figure><a href="javascript:void(0);"><img src={blogImg03}
                                                                                       alt="image description"/></a>
                                            </figure>
                                            <div className="tg-postcontent">
                                                <ul className="tg-bookscategories">
                                                    <li><a href="javascript:void(0);">Adventure</a></li>
                                                    <li><a href="javascript:void(0);">Fun</a></li>
                                                </ul>
                                                <div className="tg-themetagbox"><span
                                                    className="tg-themetag">featured</span></div>
                                                <div className="tg-posttitle">
                                                    <h3><a href="javascript:void(0);">Why Walk When You Can Climb?</a>
                                                    </h3>
                                                </div>
                                                <span className="tg-bookwriter">By: <a href="javascript:void(0);">Kathrine Culbertson</a></span>
                                                <ul className="tg-postmetadata">
                                                    <li><a href="javascript:void(0);"><i
                                                        className="fa fa-comment-o"/><i>21,415 Comments</i></a></li>
                                                    <li><a href="javascript:void(0);"><i className="fa fa-eye"/><i>24,565
                                                        Views</i></a></li>
                                                </ul>
                                            </div>
                                        </article>
                                    </div>
                                    <div className="owl-item active" style={{width: '292.5px'}}>
                                        <article className="item tg-post">
                                            <figure><a href="javascript:void(0);"><img src={blogImg03}
                                                                                       alt="image description"/></a>
                                            </figure>
                                            <div className="tg-postcontent">
                                                <ul className="tg-bookscategories">
                                                    <li><a href="javascript:void(0);">Adventure</a></li>
                                                    <li><a href="javascript:void(0);">Fun</a></li>
                                                </ul>
                                                <div className="tg-themetagbox"><span
                                                    className="tg-themetag">featured</span></div>
                                                <div className="tg-posttitle">
                                                    <h3><a href="javascript:void(0);">Where The Wild Things Are</a></h3>
                                                </div>
                                                <span className="tg-bookwriter">By: <a href="javascript:void(0);">Kathrine Culbertson</a></span>
                                                <ul className="tg-postmetadata">
                                                    <li><a href="javascript:void(0);"><i
                                                        className="fa fa-comment-o"/><i>21,415 Comments</i></a></li>
                                                    <li><a href="javascript:void(0);"><i className="fa fa-eye"/><i>24,565
                                                        Views</i></a></li>
                                                </ul>
                                            </div>
                                        </article>
                                    </div>
                                    <div className="owl-item active" style={{width: '292.5px'}}>
                                        <article className="item tg-post">
                                            <figure><a href="javascript:void(0);"><img src={blogImg03}
                                                                                       alt="image description"/></a>
                                            </figure>
                                            <div className="tg-postcontent">
                                                <ul className="tg-bookscategories">
                                                    <li><a href="javascript:void(0);">Adventure</a></li>
                                                    <li><a href="javascript:void(0);">Fun</a></li>
                                                </ul>
                                                <div className="tg-themetagbox"><span
                                                    className="tg-themetag">featured</span></div>
                                                <div className="tg-posttitle">
                                                    <h3><a href="javascript:void(0);">All She Wants To Do Is Dance</a>
                                                    </h3>
                                                </div>
                                                <span className="tg-bookwriter">By: <a href="javascript:void(0);">Kathrine Culbertson</a></span>
                                                <ul className="tg-postmetadata">
                                                    <li><a href="javascript:void(0);"><i
                                                        className="fa fa-comment-o"/><i>21,415 Comments</i></a></li>
                                                    <li><a href="javascript:void(0);"><i className="fa fa-eye"/><i>24,565
                                                        Views</i></a></li>
                                                </ul>
                                            </div>
                                        </article>
                                    </div>
                                    <div className="owl-item active" style={{width: '292.5px'}}>
                                        <article className="item tg-post">
                                            <figure><a href="javascript:void(0);"><img src={blogImg03}
                                                                                       alt="image description"/></a>
                                            </figure>
                                            <div className="tg-postcontent">
                                                <ul className="tg-bookscategories">
                                                    <li><a href="javascript:void(0);">Adventure</a></li>
                                                    <li><a href="javascript:void(0);">Fun</a></li>
                                                </ul>
                                                <div className="tg-themetagbox"><span
                                                    className="tg-themetag">featured</span></div>
                                                <div className="tg-posttitle">
                                                    <h3><a href="javascript:void(0);">Why Walk When You Can Climb?</a>
                                                    </h3>
                                                </div>
                                                <span className="tg-bookwriter">By: <a href="javascript:void(0);">Kathrine Culbertson</a></span>
                                                <ul className="tg-postmetadata">
                                                    <li><a href="javascript:void(0);"><i
                                                        className="fa fa-comment-o"/><i>21,415 Comments</i></a></li>
                                                    <li><a href="javascript:void(0);"><i className="fa fa-eye"/><i>24,565
                                                        Views</i></a></li>
                                                </ul>
                                            </div>
                                        </article>
                                    </div>
                                    <div className="owl-item active" style={{width: '292.5px'}}>
                                        <article className="item tg-post">
                                            <figure><a href="javascript:void(0);"><img src={blogImg03}
                                                                                       alt="image description"/></a>
                                            </figure>
                                            <div className="tg-postcontent">
                                                <ul className="tg-bookscategories">
                                                    <li><a href="javascript:void(0);">Adventure</a></li>
                                                    <li><a href="javascript:void(0);">Fun</a></li>
                                                </ul>
                                                <div className="tg-themetagbox"><span
                                                    className="tg-themetag">featured</span></div>
                                                <div className="tg-posttitle">
                                                    <h3><a href="javascript:void(0);">Dance Like Nobody’s Watching</a>
                                                    </h3>
                                                </div>
                                                <span className="tg-bookwriter">By: <a href="javascript:void(0);">Kathrine Culbertson</a></span>
                                                <ul className="tg-postmetadata">
                                                    <li><a href="javascript:void(0);"><i
                                                        className="fa fa-comment-o"/><i>21,415 Comments</i></a></li>
                                                    <li><a href="javascript:void(0);"><i className="fa fa-eye"/><i>24,565
                                                        Views</i></a></li>
                                                </ul>
                                            </div>
                                        </article>
                                    </div>
                                    <div className="owl-item" style={{width: '292.5px'}}>
                                        <article className="item tg-post">
                                            <figure><a href="javascript:void(0);"><img src={blogImg03}
                                                                                       alt="image description"/></a>
                                            </figure>
                                            <div className="tg-postcontent">
                                                <ul className="tg-bookscategories">
                                                    <li><a href="javascript:void(0);">Adventure</a></li>
                                                    <li><a href="javascript:void(0);">Fun</a></li>
                                                </ul>
                                                <div className="tg-themetagbox"><span
                                                    className="tg-themetag">featured</span></div>
                                                <div className="tg-posttitle">
                                                    <h3><a href="javascript:void(0);">All She Wants To Do Is Dance</a>
                                                    </h3>
                                                </div>
                                                <span className="tg-bookwriter">By: <a href="javascript:void(0);">Kathrine Culbertson</a></span>
                                                <ul className="tg-postmetadata">
                                                    <li><a href="javascript:void(0);"><i
                                                        className="fa fa-comment-o"/><i>21,415 Comments</i></a></li>
                                                    <li><a href="javascript:void(0);"><i className="fa fa-eye"/><i>24,565
                                                        Views</i></a></li>
                                                </ul>
                                            </div>
                                        </article>
                                    </div>
                                    <div className="owl-item" style={{width: '292.5px'}}>
                                        <article className="item tg-post">
                                            <figure><a href="javascript:void(0);"><img src={blogImg03}
                                                                                       alt="image description"/></a>
                                            </figure>
                                            <div className="tg-postcontent">
                                                <ul className="tg-bookscategories">
                                                    <li><a href="javascript:void(0);">Adventure</a></li>
                                                    <li><a href="javascript:void(0);">Fun</a></li>
                                                </ul>
                                                <div className="tg-themetagbox"><span
                                                    className="tg-themetag">featured</span></div>
                                                <div className="tg-posttitle">
                                                    <h3><a href="javascript:void(0);">Why Walk When You Can Climb?</a>
                                                    </h3>
                                                </div>
                                                <span className="tg-bookwriter">By: <a href="javascript:void(0);">Kathrine Culbertson</a></span>
                                                <ul className="tg-postmetadata">
                                                    <li><a href="javascript:void(0);"><i
                                                        className="fa fa-comment-o"/><i>21,415 Comments</i></a></li>
                                                    <li><a href="javascript:void(0);"><i className="fa fa-eye"/><i>24,565
                                                        Views</i></a></li>
                                                </ul>
                                            </div>
                                        </article>
                                    </div>
                                    <div className="owl-item cloned" style={{width: '292.5px'}}>
                                        <article className="item tg-post">
                                            <figure><a href="javascript:void(0);"><img src={blogImg03}
                                                                                       alt="image description"/></a>
                                            </figure>
                                            <div className="tg-postcontent">
                                                <ul className="tg-bookscategories">
                                                    <li><a href="javascript:void(0);">Adventure</a></li>
                                                    <li><a href="javascript:void(0);">Fun</a></li>
                                                </ul>
                                                <div className="tg-themetagbox"><span
                                                    className="tg-themetag">featured</span></div>
                                                <div className="tg-posttitle">
                                                    <h3><a href="javascript:void(0);">Where The Wild Things Are</a></h3>
                                                </div>
                                                <span className="tg-bookwriter">By: <a href="javascript:void(0);">Kathrine Culbertson</a></span>
                                                <ul className="tg-postmetadata">
                                                    <li><a href="javascript:void(0);"><i
                                                        className="fa fa-comment-o"/><i>21,415 Comments</i></a></li>
                                                    <li><a href="javascript:void(0);"><i className="fa fa-eye"/><i>24,565
                                                        Views</i></a></li>
                                                </ul>
                                            </div>
                                        </article>
                                    </div>
                                    <div className="owl-item cloned" style={{width: '292.5px'}}>
                                        <article className="item tg-post">
                                            <figure><a href="javascript:void(0);"><img src={blogImg03}
                                                                                       alt="image description"/></a>
                                            </figure>
                                            <div className="tg-postcontent">
                                                <ul className="tg-bookscategories">
                                                    <li><a href="javascript:void(0);">Adventure</a></li>
                                                    <li><a href="javascript:void(0);">Fun</a></li>
                                                </ul>
                                                <div className="tg-themetagbox"><span
                                                    className="tg-themetag">featured</span></div>
                                                <div className="tg-posttitle">
                                                    <h3><a href="javascript:void(0);">All She Wants To Do Is Dance</a>
                                                    </h3>
                                                </div>
                                                <span className="tg-bookwriter">By: <a href="javascript:void(0);">Kathrine Culbertson</a></span>
                                                <ul className="tg-postmetadata">
                                                    <li><a href="javascript:void(0);"><i
                                                        className="fa fa-comment-o"/><i>21,415 Comments</i></a></li>
                                                    <li><a href="javascript:void(0);"><i className="fa fa-eye"/><i>24,565
                                                        Views</i></a></li>
                                                </ul>
                                            </div>
                                        </article>
                                    </div>
                                    <div className="owl-item cloned" style={{width: '292.5px'}}>
                                        <article className="item tg-post">
                                            <figure><a href="javascript:void(0);"><img src={blogImg03}
                                                                                       alt="image description"/></a>
                                            </figure>
                                            <div className="tg-postcontent">
                                                <ul className="tg-bookscategories">
                                                    <li><a href="javascript:void(0);">Adventure</a></li>
                                                    <li><a href="javascript:void(0);">Fun</a></li>
                                                </ul>
                                                <div className="tg-themetagbox"><span
                                                    className="tg-themetag">featured</span></div>
                                                <div className="tg-posttitle">
                                                    <h3><a href="javascript:void(0);">Why Walk When You Can Climb?</a>
                                                    </h3>
                                                </div>
                                                <span className="tg-bookwriter">By: <a href="javascript:void(0);">Kathrine Culbertson</a></span>
                                                <ul className="tg-postmetadata">
                                                    <li><a href="javascript:void(0);"><i
                                                        className="fa fa-comment-o"/><i>21,415 Comments</i></a></li>
                                                    <li><a href="javascript:void(0);"><i className="fa fa-eye"/><i>24,565
                                                        Views</i></a></li>
                                                </ul>
                                            </div>
                                        </article>
                                    </div>
                                    <div className="owl-item cloned" style={{width: '292.5px'}}>
                                        <article className="item tg-post">
                                            <figure><a href="javascript:void(0);"><img src={blogImg03}
                                                                                       alt="image description"/></a>
                                            </figure>
                                            <div className="tg-postcontent">
                                                <ul className="tg-bookscategories">
                                                    <li><a href="javascript:void(0);">Adventure</a></li>
                                                    <li><a href="javascript:void(0);">Fun</a></li>
                                                </ul>
                                                <div className="tg-themetagbox"><span
                                                    className="tg-themetag">featured</span></div>
                                                <div className="tg-posttitle">
                                                    <h3><a href="javascript:void(0);">Dance Like Nobody’s Watching</a>
                                                    </h3>
                                                </div>
                                                <span className="tg-bookwriter">By: <a href="javascript:void(0);">Kathrine Culbertson</a></span>
                                                <ul className="tg-postmetadata">
                                                    <li><a href="javascript:void(0);"><i
                                                        className="fa fa-comment-o"/><i>21,415 Comments</i></a></li>
                                                    <li><a href="javascript:void(0);"><i className="fa fa-eye"/><i>24,565
                                                        Views</i></a></li>
                                                </ul>
                                            </div>
                                        </article>
                                    </div>
                                </div>
                            </div>
                            <div className="owl-nav">
                                <div className="owl-prev tg-btnround tg-btnprev"><i className="icon-chevron-left"/>
                                </div>
                                <div className="owl-next tg-btnround tg-btnnext"><i className="icon-chevron-right"/>
                                </div>
                            </div>
                            <div className="owl-dots">
                                <div className="owl-dot active"><span/></div>
                                <div className="owl-dot"><span/></div>
                            </div>
                        </div>
                    </div>
                </div>
            </section> 
            <script src = "../../js/vendor/jquery-library"></script>
            <script src="../../js/vendor/bootstrap.min.js"></script>
            <script src="https://maps.google.com/maps/api/js?key=AIzaSyCR-KEWAVCn52mSdeVeTqZjtqbmVJyfSus&amp;language=en"></script>
            <script src="../../js/owl.carousel.min.js"></script>
            <script src="../../js/jquery.vide.min.js"></script>
            <script src="../../js/countdown.js"></script>
            <script src="../../js/jquery-ui.js"></script>
            <script src="../../js/parallax.js"></script>
            <script src="../../js/countTo.js"></script>
            <script src="../../js/appear.js"></script>
            <script src="../../js/gmap3.js"></script>
            <script src="../../js/main.js"></script>
         </>


    )

}

export default Home