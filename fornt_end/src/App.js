import './App.css';
import {BrowserRouter as Router} from 'react-router-dom';
import {Route, Routes} from 'react-router-dom';
// import Home from "./pages/home/Home";
// import New from './pages/home/New';
// import Cart from './pages/home/Cart';
import Header from "./components/Header";
import Footer from "./components/Footer";
import Home from "./pages/home/Home"
// import {CartProvider} from "react-use-cart"
import './css/normalize.css';
import './css/icomoon.css';
import './css/jquery-ui.css';
import './css/owl.carousel.css';
import './css/transitions.css';
import './css/main.css';
import './css/color.css';
import './css/responsive.css';
import './css/font-awesome.min.css';

import './css/bootstrap.min.css';


function App() {

    return (
        <div className="App">
<Header/>

            {/* <Router>
        <Routes>
          <Route path="/" element={<Home />} />
        </Routes>
      </Router> */}
            {/*<CartProvider>*/}
            {/*    <New/>*/}
            {/*    <Cart/>*/}
            {/*</CartProvider>*/}
            <Home/>
 <Footer/>


        </div>
    );
}

export default App;
